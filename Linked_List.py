### Notes:
# Since only the running time of the insert method is relevant, the measuring 
# process of time complexity involves inserting an element after the second to
# last element of the linked list.
# The first attempt of uploading the linked list file was done by using the
# git command on my friend's computer, therefore another name is shown in the 
# history.

import random
import time
import matplotlib.pyplot as plt


class Node:

    def __init__(self, data):
        self.data = data
        self.next = None

    def insert(self, new_element):
        added = Node(new_element)
        added.next = self.next
        self.next = added
        linked_list[self] = self.next
        if added.next is not None:
            linked_list[added] = added.next
        return added

    def __repr__(self):
        return str(self.data)


list_length = list(range(100, 100000, 100))
average_time = []
for length in list_length:
    linked_list = {}
    average_list = list(range(1, length + 1))
    random.shuffle(average_list)
    i = 0
    while i < len(average_list) - 1:
        a = Node(average_list[i])
        b = a.insert(average_list[i + 1])
        i += 1
    start = time.time()
    c = a.insert(0)
    end = time.time()
    average_time.append(end - start)

plt.plot(list_length, average_time)
plt.show()